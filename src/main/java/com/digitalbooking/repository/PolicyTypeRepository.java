package com.digitalbooking.repository;

import com.digitalbooking.model.entity.PolicyType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PolicyTypeRepository extends JpaRepository<PolicyType, Long> {
    boolean existsByName(String name);
}