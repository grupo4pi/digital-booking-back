package com.digitalbooking.repository;

import com.digitalbooking.model.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ImageRepository extends JpaRepository<Image, Long> {
    boolean existsByUrl(String url);

    List<Image> findByProductId(Long productId);
}
