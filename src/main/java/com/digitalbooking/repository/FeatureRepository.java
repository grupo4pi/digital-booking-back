package com.digitalbooking.repository;

import com.digitalbooking.model.entity.Feature;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FeatureRepository extends JpaRepository<Feature, Long> {
    List<Feature> findFeaturesByProductsId(Long id);

    boolean existsByName(String name);

    boolean existsByIcon(String icon);
}
