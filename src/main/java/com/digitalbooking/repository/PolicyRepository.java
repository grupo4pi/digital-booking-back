package com.digitalbooking.repository;

import com.digitalbooking.model.entity.Policy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PolicyRepository extends JpaRepository<Policy, Long> {
    Optional<Policy> findByName(String name);

    boolean existsByName(String name);

    List<Policy> findByProductId(Long productId);
}
