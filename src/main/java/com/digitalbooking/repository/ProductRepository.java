package com.digitalbooking.repository;

import com.digitalbooking.model.entity.Product;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {
    @EntityGraph(attributePaths = {"category", "city", "images", "policies.policyType", "features"}, type = EntityGraph.EntityGraphType.LOAD)
    Optional<Product> findProductById(Long id);

    Optional<Product> findByName(String name);

    boolean existsByName(String name);

    List<Product> findByCityId(Long cityId);

    List<Product> findByCategoryId(Long categoryId);
}
