package com.digitalbooking.exception;

public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String resource, String field, String value) {
        super(String.format("No existe un recurso (%s) con %s %s", resource, field, value));
    }
}
