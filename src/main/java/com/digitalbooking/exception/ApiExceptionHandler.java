package com.digitalbooking.exception;

import com.digitalbooking.model.response.ResponsePayload;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.HashMap;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(value = {ResourceNotFoundException.class, DuplicatedValueException.class, MethodArgumentNotValidException.class, MethodArgumentTypeMismatchException.class})
    public ResponseEntity<ResponsePayload> handleException(Exception e) {
        if (e instanceof ResourceNotFoundException ex) {
            return handleResourceNotFoundException(ex);
        }

        if (e instanceof DuplicatedValueException ex) {
            return handleDuplicatedValueException(ex);
        }

        if (e instanceof MethodArgumentNotValidException ex) {
            return handleMethodArgumentNotValidException(ex);
        }

        if (e instanceof MethodArgumentTypeMismatchException ex) {
            return handleMethodArgumentTypeMismatchException(ex);
        }

        return null;
    }

    @ResponseStatus(NOT_FOUND)
    private ResponseEntity<ResponsePayload> handleResourceNotFoundException(ResourceNotFoundException e) {
        ResponsePayload payload = new ResponsePayload(e.getMessage());
        return handleInternalException(payload, NOT_FOUND);
    }

    @ResponseStatus(BAD_REQUEST)
    private ResponseEntity<ResponsePayload> handleDuplicatedValueException(DuplicatedValueException e) {
        ResponsePayload payload = new ResponsePayload(e.getMessage());
        return handleInternalException(payload, BAD_REQUEST);
    }

    @ResponseStatus(BAD_REQUEST)
    private ResponseEntity<ResponsePayload> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        HashMap<String, Object> errors = new HashMap<>();

        e.getBindingResult()
                .getFieldErrors()
                .forEach(error ->
                        errors.put(error.getField(), error.getDefaultMessage())
                );

        ResponsePayload payload = new ResponsePayload("Los datos enviados presentan errores", errors);
        return handleInternalException(payload, BAD_REQUEST);
    }

    @ResponseStatus(BAD_REQUEST)
    private ResponseEntity<ResponsePayload> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
        ResponsePayload responsePayload = new ResponsePayload("El tipo de dato del argumento no concuerda con el esperado", BAD_REQUEST);
        return handleInternalException(responsePayload, BAD_REQUEST);
    }

    private ResponseEntity<ResponsePayload> handleInternalException(ResponsePayload payload, HttpStatus status) {
        return new ResponseEntity<>(payload, status);
    }
}
