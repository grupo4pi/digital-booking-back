package com.digitalbooking.exception;

public class DuplicatedValueException extends RuntimeException {
    public DuplicatedValueException(String resource, String field) {
        super(String.format("Ya existe un recurso (%s) con el mismo atributo %s", resource, field));
    }
}
