package com.digitalbooking.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
public class ResponsePayload {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object data;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, Object> errors;

    public ResponsePayload(String message, Object data) {
        this.message = message;
        this.data = data;
    }

    public ResponsePayload(String message, Map<String, Object> errors) {
        this.message = message;
        this.errors = errors;
    }

    public ResponsePayload(String message) {
        this.message = message;
    }

    public ResponsePayload(Object data) {
        this.data = data;
    }
}
