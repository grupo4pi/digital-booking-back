package com.digitalbooking.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@Getter
@Setter
public class PolicyTypeDto {
    private Long id;

    @NotEmpty(message = "El nombre es obligatorio")
    private String name;
}