package com.digitalbooking.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDto {
    private Long id;

    @NotBlank(message = "El título de la categoría es obligatorio")
    @Size(max = 100, message = "El título debe tener entre 1 y 100 caracteres")
    private String name;

    @NotBlank(message = "La descripción de la categoría es obligatoria")
    private String description;

    @NotBlank(message = "La url de la imagen de la categoría es obligatoria")
    @URL(message = "La url es inválida")
    private String imageUrl;
}
