package com.digitalbooking.model.dto;

import lombok.*;

import javax.validation.constraints.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class CityDto {
    private Long id;

    @NonNull
    @NotBlank(message = "El nombre de la ciudad es obligatoria")
    @Size(max = 100, message = "El nombre debe tener entre 1 y 100 caracteres")
    private String name;

    @NonNull
    @NotBlank(message = "El país es obligatorio")
    @Size(max = 100, message = "El nombre debe tener entre 1 y 100 caracteres")
    private String country;

    @NonNull
    @NotNull(message = "La latitud es obligatoria")
    private Double latitude;

    @NonNull
    @NotNull(message = "La longitud es obligatoria")
    private Double longitude;
}
