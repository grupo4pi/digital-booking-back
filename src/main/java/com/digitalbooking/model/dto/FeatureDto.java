package com.digitalbooking.model.dto;

import lombok.*;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class FeatureDto {
    private Long id;

    @NonNull
    @NotBlank(message = "Icono obligatorio")
    @URL(message = "La url del ícono es inválida")
    private String icon;

    @NonNull
    @NotBlank(message = "El nombre de la caracteristica es obligatoria")
    @Size(max = 50, message = "El nombre debe tener entre 1 y 50 caracteres")
    private String name;
}
