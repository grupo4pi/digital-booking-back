package com.digitalbooking.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {
    private Long id;

    @NotNull(message = "El precio del producto es obligatorio")
    @Min(value = 0, message = "El precio debe ser mayor a cero")
    private Integer price;

    @NotBlank(message = "El título del producto es obligatorio")
    @Size(max = 100, message = "El título debe tener entre 1 y 100 caracteres")
    private String name;

    @NotNull(message = "La disponibilidad del producto no puede estar vacía")
    private Boolean availability;

    @NotBlank(message = "Debes agregar un titulo descriptivo del producto")
    @Size(max = 100, message = "El título debe tener entre 1 y 100 caracteres")
    private String descriptionTitle;

    @NotBlank(message = "Debes agregarle una descripcion del producto")
    @Size(max = 255, message = "El título debe tener entre 1 y 255 caracteres")
    private String description;

    @NotNull(message = "La categoría es obligatoria")
    private Long category;

    @NotNull(message = "La ciudad es oblitagoria")
    private Long city;
}
