package com.digitalbooking.model.dto;

import lombok.*;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class ImageDto {
    private Long id;

    @NonNull
    @NotBlank(message = "El nombre de la imagen es obligatorio")
    @Size(max = 100, message = "El nombre debe tener entre 1 y 100 caracteres")
    private String name;

    @NonNull
    @NotBlank(message = "La url de la imagen es obligatoria")
    @URL(message = "La url de la imágen es inválida")
    private String url;

    private ProductDto product;
}
