package com.digitalbooking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
@OnDelete(action = OnDeleteAction.CASCADE)
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, columnDefinition = "MEDIUMINT UNSIGNED")
    private Long id;

    @NonNull
    @Column(nullable = false, columnDefinition = "INT")
    private Integer price;

    @NonNull
    @Column(nullable = false, columnDefinition = "VARCHAR(100)", length = 100, unique = true)
    private String name;

    @NonNull
    @Column(nullable = false)
    private Boolean availability;

    @Column(nullable = false, columnDefinition = "VARCHAR(100)", length = 100)
    private String descriptionTitle;

    @NonNull
    @Lob
    @Column(nullable = false, columnDefinition = "TEXT")
    private String description;

    @NonNull
    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @NonNull
    @OneToOne
    @JoinColumn(name = "city_id")
    private City city;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @OneToMany(mappedBy = "product")
    private Set<Image> images = new HashSet<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @OneToMany(mappedBy = "product")
    private Set<Policy> policies = new HashSet<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "product_features",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "feature_id"))
    private Set<Feature> features = new HashSet<>();

    public void addFeature(Feature feature) {
        this.features.add(feature);
        feature.getProducts().add(this);
    }

    public void removeFeature(Feature feature) {
        this.features.remove(feature);
        feature.getProducts().remove(this);
    }

    public void addImage(Image image) {
        this.images.add(image);
        image.setProduct(this);
    }

    public void removeImage(Image image) {
        this.images.remove(image);
    }

    public void addPolicy(Policy policy) {
        this.policies.add(policy);
        policy.setProduct(this);
    }

    public void removePolicy(Policy policy) {
        this.policies.remove(policy);
    }
}
