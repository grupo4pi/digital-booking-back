package com.digitalbooking.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PolicyType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, columnDefinition = "TINYINT UNSIGNED")
    private Long id;

    @Column(nullable = false, columnDefinition = "VARCHAR(100)", length = 100, unique = true)
    private String name;
}