package com.digitalbooking.model.entity;

import lombok.*;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, columnDefinition = "MEDIUMINT UNSIGNED")
    private Long id;

    @NonNull
    @Column(nullable = false, columnDefinition = "VARCHAR(100)", length = 100)
    private String name;

    @NonNull
    @Column(nullable = false, columnDefinition = "VARCHAR(100)", length = 100)
    private String country;

    @NonNull
    @Column(nullable = false)
    private Double latitude;

    @NonNull
    @Column(nullable = false)
    private Double longitude;
}
