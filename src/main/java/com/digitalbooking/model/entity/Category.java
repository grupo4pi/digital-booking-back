package com.digitalbooking.model.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "uq_name", columnNames = "name")
})
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, columnDefinition = "TINYINT UNSIGNED")
    private Long id;

    @NonNull
    @Column(nullable = false, columnDefinition = "VARCHAR(100)", length = 100)
    private String name;

    @NonNull
    @Column(nullable = false, columnDefinition = "VARCHAR(100)", length = 100)
    private String description;

    @NonNull
    @Column(nullable = false, columnDefinition = "VARCHAR(255)")
    private String imageUrl;
}

