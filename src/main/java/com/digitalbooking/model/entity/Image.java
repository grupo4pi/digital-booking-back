package com.digitalbooking.model.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "uq_url", columnNames = "url")
})
@Entity
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, columnDefinition = "MEDIUMINT UNSIGNED")
    private Long id;

    @NonNull
    @Column(nullable = false, columnDefinition = "VARCHAR(100)", length = 100)
    private String name;

    @NonNull
    @Column(nullable = false, columnDefinition = "VARCHAR(255)")
    private String url;

    @ManyToOne(optional = false)
    @JoinColumn(name = "product_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Product product;
}
