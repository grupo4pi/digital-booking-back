package com.digitalbooking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "uq_name", columnNames = "name"),
        @UniqueConstraint(name = "uq_icon", columnNames = "icon")
})
@OnDelete(action = OnDeleteAction.CASCADE)
public class Feature {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, columnDefinition = "MEDIUMINT UNSIGNED")
    private Long id;

    @NonNull
    @Column(nullable = false, columnDefinition = "VARCHAR(100)", length = 100)
    private String icon;

    @NonNull
    @Column(nullable = false, columnDefinition = "VARCHAR(100)", length = 100)
    private String name;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "features")
    @JsonIgnore
    private Set<Product> products = new HashSet<>();
}
