package com.digitalbooking.service;

import com.digitalbooking.model.dto.PolicyDto;
import com.digitalbooking.model.entity.Policy;

import java.util.List;

public interface PolicyService {
    Policy updateById(Long id, PolicyDto policyDto);

    void deleteById(Long id);

    List<Policy> findAllByProductId(Long productId);

    Policy createPolicy(Long productId, Policy policy);
}
