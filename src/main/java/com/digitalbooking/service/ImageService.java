package com.digitalbooking.service;

import com.digitalbooking.model.dto.ImageDto;
import com.digitalbooking.model.entity.Image;

import java.util.List;

public interface ImageService {
    Image updateById(Long id, ImageDto imageDto);

    List<Image> getAllImagesByProductId(Long productId);

    Image createImage(Long productId, Image image);

    void deleteById(Long id);
}
