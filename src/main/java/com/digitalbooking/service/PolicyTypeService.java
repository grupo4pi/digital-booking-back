package com.digitalbooking.service;

import com.digitalbooking.model.dto.PolicyTypeDto;
import com.digitalbooking.model.entity.PolicyType;

import java.util.List;

public interface PolicyTypeService {
    List<PolicyType> findAll();

    PolicyType save(PolicyType policyType);

    PolicyType updateById(Long id, PolicyTypeDto policyTypeDto);

    void deleteById(Long id);
}
