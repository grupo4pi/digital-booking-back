package com.digitalbooking.service;

import com.digitalbooking.model.dto.ProductDto;
import com.digitalbooking.model.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> findAll();

    Product findById(Long id);

    Product save(ProductDto productDto);

    List<Product> findAllByCityId(Long cityId);

    List<Product> findAllByCategoryId(Long categoryId);

    void deleteById(Long id);

    Product updateById(Long id, ProductDto productDto);
}
