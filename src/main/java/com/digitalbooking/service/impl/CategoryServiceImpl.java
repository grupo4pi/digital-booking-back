package com.digitalbooking.service.impl;

import com.digitalbooking.exception.DuplicatedValueException;
import com.digitalbooking.exception.ResourceNotFoundException;
import com.digitalbooking.model.dto.CategoryDto;
import com.digitalbooking.model.entity.Category;
import com.digitalbooking.repository.CategoryRepository;
import com.digitalbooking.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository repository;
    private static final String RESOURCE_NAME = "categoría";

    @Override
    public Category save(Category category) {
        boolean existsByName = repository.existsByName(category.getName());

        if (existsByName) {
            throw new DuplicatedValueException(RESOURCE_NAME, "nombre");
        }

        return repository.save(category);
    }

    @Override
    public List<Category> findAll() {
        return repository.findAll();
    }

    @Override
    public Category findById(Long id) {
        return repository
                .findById(id)
                .orElseThrow(
                        () -> new ResourceNotFoundException(RESOURCE_NAME, "id", id.toString())
                );
    }

    @Override
    @Transactional
    public Category updateById(Long id, CategoryDto categoryDto) {
        String name = categoryDto.getName();
        String description = categoryDto.getDescription();
        String url = categoryDto.getImageUrl();

        Category category = repository
                .findById(id)
                .orElseThrow(
                        () -> new ResourceNotFoundException(RESOURCE_NAME, "id", id.toString())
                );

        if (!Objects.equals(category.getName(), name)) {
            if (repository.existsByName(name)) {
                throw new DuplicatedValueException(RESOURCE_NAME, "nombre");
            }

            category.setName(name);
        }

        if (!Objects.equals(category.getDescription(), description)) {
            category.setDescription(description);
        }

        if (!Objects.equals(category.getImageUrl(), url)) {
            category.setImageUrl(url);
        }

        return category;
    }

    @Override
    public void deleteById(Long id) {
        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException(RESOURCE_NAME, "id", id.toString());
        }
    }
}
