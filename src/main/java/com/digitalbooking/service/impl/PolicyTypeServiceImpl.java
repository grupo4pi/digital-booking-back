package com.digitalbooking.service.impl;

import com.digitalbooking.exception.DuplicatedValueException;
import com.digitalbooking.exception.ResourceNotFoundException;
import com.digitalbooking.model.dto.PolicyTypeDto;
import com.digitalbooking.model.entity.PolicyType;
import com.digitalbooking.repository.PolicyTypeRepository;
import com.digitalbooking.service.PolicyTypeService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PolicyTypeServiceImpl implements PolicyTypeService {
    private final PolicyTypeRepository repository;
    private static final String RESOURCE_NAME = "tipo de política";

    @Override
    public List<PolicyType> findAll() {
        return repository.findAll();
    }

    @Override
    public PolicyType save(PolicyType policyType) {
        if (repository.existsByName(policyType.getName())) {
            throw new DuplicatedValueException(RESOURCE_NAME, "nombre");
        }

        return repository.save(policyType);
    }

    @Override
    @Transactional
    public PolicyType updateById(Long id, PolicyTypeDto policyTypeDto) {
        PolicyType policyType = repository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(RESOURCE_NAME, "id", id.toString()));

        String name = policyTypeDto.getName();

        if (!Objects.equals(policyType.getName(), name)) {
            if (repository.existsByName(name)) {
                throw new DuplicatedValueException(RESOURCE_NAME, "nombre");
            }

            policyType.setName(name);
        }

        return policyType;
    }

    @Override
    public void deleteById(Long id) {
        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException(RESOURCE_NAME, "id", id.toString());
        }
    }
}
