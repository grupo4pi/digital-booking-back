package com.digitalbooking.service.impl;

import com.digitalbooking.exception.ResourceNotFoundException;
import com.digitalbooking.model.dto.PolicyDto;
import com.digitalbooking.model.entity.Policy;
import com.digitalbooking.model.entity.Product;
import com.digitalbooking.repository.PolicyRepository;
import com.digitalbooking.repository.PolicyTypeRepository;
import com.digitalbooking.repository.ProductRepository;
import com.digitalbooking.service.PolicyService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PolicyServiceImpl implements PolicyService {
    private final PolicyRepository repository;
    private final ProductRepository productRepository;
    private static final String RESOURCE_NAME = "política";

    @Override
    @Transactional
    public Policy updateById(Long id, PolicyDto policyDto) {
        Policy policy = repository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(RESOURCE_NAME, "id", id.toString()));

        String name = policyDto.getName();

        if (!Objects.equals(policy.getName(), name)) {
            policy.setName(name);
        }

        return policy;
    }

    @Override
    public void deleteById(Long id) {
        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException(RESOURCE_NAME, "id", id.toString());
        }
    }

    @Override
    public List<Policy> findAllByProductId(Long productId) {
        return repository.findByProductId(productId);
    }

    @Override
    public Policy createPolicy(Long productId, Policy policy) {
        Product product = productRepository
                .findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("producto", "id", productId.toString()));

        product.addPolicy(policy);
        return repository.save(policy);
    }
}
