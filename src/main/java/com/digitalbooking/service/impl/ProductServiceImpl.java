package com.digitalbooking.service.impl;

import com.digitalbooking.exception.DuplicatedValueException;
import com.digitalbooking.exception.ResourceNotFoundException;
import com.digitalbooking.model.dto.ProductDto;
import com.digitalbooking.model.entity.Category;
import com.digitalbooking.model.entity.City;
import com.digitalbooking.model.entity.Product;
import com.digitalbooking.repository.CategoryRepository;
import com.digitalbooking.repository.CityRepository;
import com.digitalbooking.repository.ProductRepository;
import com.digitalbooking.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ProductServiceImpl implements ProductService {
    private final ProductRepository repository;
    private final CityRepository cityRepository;
    private final CategoryRepository categoryRepository;

    private static final String RESOURCE_NAME = "producto";

    @Transactional
    @Override
    public Product updateById(Long id, ProductDto productDto) {
        Product product = repository
                .findById(id)
                .orElseThrow(
                        () -> new ResourceNotFoundException(RESOURCE_NAME, "id", id.toString())
                );

        City city = cityRepository
                .findById(productDto.getCity())
                .orElseThrow(
                        () -> new ResourceNotFoundException("ciudad", "id", productDto.getCity().toString())
                );

        Category category = categoryRepository
                .findById(productDto.getCategory())
                .orElseThrow(
                        () -> new ResourceNotFoundException("categoría", "id", productDto.getCategory().toString())
                );

        Integer price = productDto.getPrice();
        String name = productDto.getName();
        Boolean availability = productDto.getAvailability();
        String description = productDto.getDescription();
        String descriptionTitle = productDto.getDescriptionTitle();

        if (!Objects.equals(product.getPrice(), price)) {
            product.setPrice(price);
        }

        if (!Objects.equals(product.getName(), name)) {
            if (repository.existsByName(name)) {
                throw new DuplicatedValueException(RESOURCE_NAME, "nombre");
            }

            product.setName(name);
        }

        if (!Objects.equals(product.getAvailability(), availability)) {
            product.setAvailability(availability);
        }

        if (!Objects.equals(product.getDescription(), description)) {
            product.setDescription(description);
        }

        if (!Objects.equals(product.getDescriptionTitle(), descriptionTitle)) {
            product.setDescriptionTitle(descriptionTitle);
        }

        if (!Objects.equals(product.getCity().getId(), city.getId())) {
            product.setCity(city);
        }

        if (!Objects.equals(product.getCategory().getId(), category.getId())) {
            product.setCategory(category);
        }

        return product;
    }

    @Override
    public List<Product> findAll() {
        return repository.findAll();
    }

    @Override
    public Product findById(Long id) {
        return repository
                .findProductById(id)
                .orElseThrow(
                        () -> new ResourceNotFoundException(RESOURCE_NAME, "id", id.toString())
                );
    }

    @Override
    public Product save(ProductDto productDto) {
        City city = cityRepository
                .findById(productDto.getCity())
                .orElseThrow(
                        () -> new ResourceNotFoundException("ciudad", "id", productDto.getCity().toString())
                );

        Category category = categoryRepository
                .findById(productDto.getCategory())
                .orElseThrow(
                        () -> new ResourceNotFoundException("categoría", "id", productDto.getCategory().toString())
                );

        Product product = new Product();
        product.setName(productDto.getName());
        product.setPrice(productDto.getPrice());
        product.setAvailability(productDto.getAvailability());
        product.setDescription(productDto.getDescription());
        product.setDescriptionTitle(productDto.getDescriptionTitle());
        product.setCity(city);
        product.setCategory(category);

        return repository.save(product);
    }

    @Override
    public List<Product> findAllByCityId(Long cityId) {
        return repository.findByCityId(cityId);
    }

    @Override
    public List<Product> findAllByCategoryId(Long categoryId) {
        return repository.findByCategoryId(categoryId);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
