package com.digitalbooking.service.impl;

import com.digitalbooking.exception.ResourceNotFoundException;
import com.digitalbooking.model.dto.CityDto;
import com.digitalbooking.model.entity.City;
import com.digitalbooking.repository.CityRepository;
import com.digitalbooking.service.CityService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class CityServiceImpl implements CityService {
    private final CityRepository repository;
    private static final String RESOURCE_NAME = "ciudad";

    @Override
    public List<City> findAll() {
        return repository.findAll();
    }

    @Override
    public City save(City city) {
        return repository.save(city);
    }

    @Override
    @Transactional
    public City updateById(Long id, CityDto cityDto) {
        String name = cityDto.getName();
        String country = cityDto.getCountry();
        Double latitude = cityDto.getLatitude();
        Double longitude = cityDto.getLongitude();

        City city = repository
                .findById(id)
                .orElseThrow(
                        () -> new ResourceNotFoundException(RESOURCE_NAME, "id", id.toString())
                );

        if (!Objects.equals(city.getName(), name)) {
            city.setName(name);
        }

        if (!Objects.equals(city.getCountry(), country)) {
            city.setCountry(country);
        }

        if (!Objects.equals(city.getLatitude(), latitude)) {
            city.setLatitude(latitude);
        }

        if (!Objects.equals(city.getLongitude(), longitude)) {
            city.setLatitude(longitude);
        }

        return city;
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
