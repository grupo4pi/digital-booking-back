package com.digitalbooking.service.impl;

import com.digitalbooking.exception.DuplicatedValueException;
import com.digitalbooking.exception.ResourceNotFoundException;
import com.digitalbooking.model.dto.ImageDto;
import com.digitalbooking.model.entity.Image;
import com.digitalbooking.model.entity.Product;
import com.digitalbooking.repository.ImageRepository;
import com.digitalbooking.repository.ProductRepository;
import com.digitalbooking.service.ImageService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ImageServiceImpl implements ImageService {
    private final ProductRepository productRepository;
    private final ImageRepository repository;
    private static final String RESOURCE_NAME = "imagen";

    @Override
    @Transactional
    public Image updateById(Long id, ImageDto imageDto) {
        String name = imageDto.getName();
        String url = imageDto.getUrl();

        Image image = repository
                .findById(id)
                .orElseThrow(
                        () -> new ResourceNotFoundException(RESOURCE_NAME, "id", id.toString())
                );

        if (!Objects.equals(image.getUrl(), url)) {
            boolean existsByUrl = repository.existsByUrl(url);

            if (existsByUrl) {
                throw new DuplicatedValueException(RESOURCE_NAME, "url");
            }

            image.setUrl(url);
        }

        if (!Objects.equals(image.getName(), name)) {
            image.setName(name);
        }

        return image;
    }

    @Override
    public List<Image> getAllImagesByProductId(Long productId) {
        if (!productRepository.existsById(productId)) {
            throw new ResourceNotFoundException("product", "id", productId.toString());
        }

        return repository.findByProductId(productId);
    }

    @Override
    public Image createImage(Long productId, Image image) {
        if (repository.existsByUrl(image.getUrl())) {
            throw new DuplicatedValueException(RESOURCE_NAME, "url");
        }

        Product product = productRepository
                .findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("producto", "id", productId.toString()));

        product.addImage(image);
        return repository.save(image);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
