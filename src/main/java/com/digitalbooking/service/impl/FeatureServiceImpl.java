package com.digitalbooking.service.impl;

import com.digitalbooking.exception.DuplicatedValueException;
import com.digitalbooking.exception.ResourceNotFoundException;
import com.digitalbooking.model.dto.FeatureDto;
import com.digitalbooking.model.entity.Feature;
import com.digitalbooking.model.entity.Product;
import com.digitalbooking.repository.FeatureRepository;
import com.digitalbooking.repository.ProductRepository;
import com.digitalbooking.service.FeatureService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class FeatureServiceImpl implements FeatureService {
    private final FeatureRepository repository;
    private final ProductRepository productRepository;
    private static final String RESOURCE_NAME = "característica";
    private static final String PRODUCT_RESOURCE_NAME = "producto";

    @Override
    public Feature save(Feature feature) {
        boolean existsByName = repository.existsByName(feature.getName());

        if (existsByName) {
            throw new DuplicatedValueException(RESOURCE_NAME, "nombre");
        }

        return repository.save(feature);
    }

    @Override
    @Transactional
    public Feature updateById(Long id, FeatureDto featureDto) {
        String name = featureDto.getName();
        String icon = featureDto.getIcon();

        Feature feature = repository
                .findById(id)
                .orElseThrow(
                        () -> new ResourceNotFoundException(RESOURCE_NAME, "id", id.toString())
                );

        /*
        Checks if the new data is the same as the actual data and
        performs constraints checks to update data
         */
        if (!Objects.equals(feature.getName(), name)) {
            if (repository.existsByName(name)) {
                throw new DuplicatedValueException(RESOURCE_NAME, "nombre");
            }

            feature.setName(name);
        }

        if (!Objects.equals(feature.getIcon(), icon)) {
            if (repository.existsByIcon(icon)) {
                throw new DuplicatedValueException(RESOURCE_NAME, "icono");
            }

            feature.setIcon(icon);
        }

        return feature;
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<Feature> getAllFeaturesByProduct(Long id) {
        if (!productRepository.existsById(id)) {
            throw new ResourceNotFoundException(PRODUCT_RESOURCE_NAME, "id", id.toString());
        }

        return repository.findFeaturesByProductsId(id);
    }

    @Override
    @Transactional
    public Feature createAndAddFeatureToProduct(Long productId, Feature feature) {
        Product product = productRepository
                .findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException(PRODUCT_RESOURCE_NAME, "id", productId.toString()));

        // Validate entity constraints
        if (repository.existsByIcon(feature.getIcon())) {
            throw new DuplicatedValueException(RESOURCE_NAME, "icono");
        }

        if (repository.existsByName(feature.getName())) {
            throw new DuplicatedValueException(RESOURCE_NAME, "nombre");
        }

        // Add feature to product and save feature
        product.addFeature(feature);
        return repository.save(feature);
    }

    @Override
    public Feature addFeatureToProduct(Long productId, Long featureId) {
        // Checks the existence of the entities
        Product product = productRepository
                .findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException(PRODUCT_RESOURCE_NAME, "id", productId.toString()));

        Feature feature = repository
                .findById(featureId)
                .orElseThrow(() -> new ResourceNotFoundException(RESOURCE_NAME, "id", featureId.toString()));

        // Add feature to product and save product
        product.addFeature(feature);
        productRepository.save(product);
        return feature;
    }

    @Override
    public void removeFeatureFromProduct(Long productId, Long featureId) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException(PRODUCT_RESOURCE_NAME, "id", productId.toString()));

        Feature feature = repository.findById(featureId)
                .orElseThrow(() -> new ResourceNotFoundException(RESOURCE_NAME, "id", featureId.toString()));

        // Remove the feature from product and save
        product.removeFeature(feature);
        productRepository.save(product);
    }
}
