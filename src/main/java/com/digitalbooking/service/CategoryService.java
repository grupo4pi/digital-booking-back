package com.digitalbooking.service;

import com.digitalbooking.model.dto.CategoryDto;
import com.digitalbooking.model.entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();

    Category findById(Long id);

    Category save(Category category);

    Category updateById(Long id, CategoryDto categoryDto);

    void deleteById(Long id);
}
