package com.digitalbooking.service;

import com.digitalbooking.model.dto.FeatureDto;
import com.digitalbooking.model.entity.Feature;

import java.util.List;

public interface FeatureService {
    Feature save(Feature feature);

    Feature updateById(Long id, FeatureDto featureDto);

    void deleteById(Long id);

    List<Feature> getAllFeaturesByProduct(Long id);

    Feature createAndAddFeatureToProduct(Long productId, Feature feature);

    Feature addFeatureToProduct(Long productId, Long featureId);

    void removeFeatureFromProduct(Long productId, Long featureId);
}
