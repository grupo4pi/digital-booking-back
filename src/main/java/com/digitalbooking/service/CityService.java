package com.digitalbooking.service;

import com.digitalbooking.model.dto.CityDto;
import com.digitalbooking.model.entity.City;

import java.util.List;

public interface CityService {
    List<City> findAll();

    City save(City city);

    City updateById(Long id, CityDto cityDto);

    void deleteById(Long id);
}
