package com.digitalbooking.controller;


import com.digitalbooking.model.dto.CityDto;
import com.digitalbooking.model.entity.City;
import com.digitalbooking.model.response.ResponsePayload;
import com.digitalbooking.service.CityService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/v1", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CityController {
    private final CityService service;
    private final ModelMapper modelMapper;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/cities")
    public ResponseEntity<ResponsePayload> findAll() {
        return ResponseEntity.ok(new ResponsePayload(service.findAll()));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/cities", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> save(@RequestBody @Valid CityDto cityDto) {
        ResponsePayload responsePayload =
                new ResponsePayload("La ciudad ha sido creada",
                        service.save(convertToEntity(cityDto)));

        return new ResponseEntity<>(responsePayload, HttpStatus.CREATED);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/cities/{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> updateById(@PathVariable Long id, @RequestBody @Valid CityDto cityDto) {
        ResponsePayload responsePayload =
                new ResponsePayload("La ciudad ha sido actualizada",
                        service.updateById(id, cityDto));

        return ResponseEntity.ok(responsePayload);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/cities/{id}")
    public ResponseEntity<ResponsePayload> deleteById(@PathVariable Long id) {
        service.deleteById(id);
        return ResponseEntity.ok(new ResponsePayload(String.format("La ciudad con id %s ha sido eliminada", id.toString())));
    }

    private City convertToEntity(CityDto cityDto) {
        return modelMapper.map(cityDto, City.class);
    }
}
