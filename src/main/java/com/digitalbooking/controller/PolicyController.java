package com.digitalbooking.controller;

import com.digitalbooking.model.dto.PolicyDto;
import com.digitalbooking.model.entity.Policy;
import com.digitalbooking.model.response.ResponsePayload;
import com.digitalbooking.service.PolicyService;
import lombok.AllArgsConstructor;
import org.apache.coyote.Response;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/v1", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PolicyController {
    private final PolicyService service;
    private final ModelMapper modelMapper;

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = "/policies/{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> updateById(@PathVariable Long id, @RequestBody @Valid PolicyDto policyDto) {
        ResponsePayload responsePayload = new ResponsePayload(
                "La política ha sido actualizada",
                service.updateById(id, policyDto));

        return ResponseEntity.ok(responsePayload);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/policies/{id}")
    public ResponseEntity<ResponsePayload> deleteById(@PathVariable Long id) {
        service.deleteById(id);
        return ResponseEntity.ok(new ResponsePayload("La política ha sido eliminada"));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/products/{productId}/policies")
    public ResponseEntity<ResponsePayload> findAllByProductId(@PathVariable Long productId) {
        return ResponseEntity.ok(new ResponsePayload(service.findAllByProductId(productId)));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/products/{productId}/policies", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> createPolicy(@PathVariable Long productId, @RequestBody @Valid PolicyDto policyDto) {
        ResponsePayload responsePayload = new ResponsePayload(
                "La política ha sido creada",
                service.createPolicy(productId, convertToEntity(policyDto)));

        return new ResponseEntity<>(responsePayload, HttpStatus.CREATED);
    }

    private Policy convertToEntity(PolicyDto policyDto) {
        return modelMapper.map(policyDto, Policy.class);
    }
}
