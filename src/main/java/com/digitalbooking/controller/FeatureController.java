package com.digitalbooking.controller;

import com.digitalbooking.model.dto.FeatureDto;
import com.digitalbooking.model.entity.Feature;
import com.digitalbooking.model.response.ResponsePayload;
import com.digitalbooking.service.FeatureService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/v1", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class FeatureController {
    private final FeatureService service;
    private final ModelMapper modelMapper;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/features", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> save(@RequestBody @Valid FeatureDto featureDto) {
        ResponsePayload responsePayload =
                new ResponsePayload("La característica ha sido creada",
                        service.save(convertToEntity(featureDto)));

        return new ResponseEntity<>(responsePayload, HttpStatus.CREATED);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = "/features/{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> updateById(@PathVariable Long id, @RequestBody @Valid FeatureDto featureDto) {
        service.updateById(id, featureDto);
        return ResponseEntity.ok(new ResponsePayload(String.format("La característica con id %s ha sido actualizada", id.toString())));
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/features/{id}")
    public ResponseEntity<ResponsePayload> deleteById(@PathVariable Long id) {
        service.deleteById(id);
        return ResponseEntity.ok(new ResponsePayload(String.format("La característica con id %s ha sido eliminada", id.toString())));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/products/{productId}/features")
    public ResponseEntity<ResponsePayload> getAllFeaturesByProductId(@PathVariable Long productId) {
        return ResponseEntity.ok(new ResponsePayload(service.getAllFeaturesByProduct(productId)));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/products/{productId}/features", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> createAndAddFeatureToProduct(@PathVariable Long productId, @RequestBody @Valid FeatureDto featureDto) {
        ResponsePayload responsePayload = new ResponsePayload(
                String.format("La característica ha sido creada y añadida al producto con id %s", productId.toString()),
                service.createAndAddFeatureToProduct(productId, convertToEntity(featureDto))
        );

        return new ResponseEntity<>(responsePayload, HttpStatus.CREATED);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/products/{productId}/features/{featureId}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> addFeatureToProduct(@PathVariable Long productId, @PathVariable Long featureId) {
        ResponsePayload responsePayload = new ResponsePayload(
                String.format("La característica con id %s ha sido añadida al producto con id %s", featureId, productId),
                service.addFeatureToProduct(productId, featureId)
        );

        return new ResponseEntity<>(responsePayload, HttpStatus.CREATED);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/products/{productId}/features/{featureId}")
    public ResponseEntity<ResponsePayload> removeFeatureFromProduct(@PathVariable Long productId, @PathVariable Long featureId) {
        service.removeFeatureFromProduct(productId, featureId);
        return ResponseEntity.ok(new ResponsePayload(String.format("La característica con id %s ha sido removida del producto con id %s", featureId, productId)));
    }

    private Feature convertToEntity(FeatureDto featureDto) {
        return modelMapper.map(featureDto, Feature.class);
    }
}
