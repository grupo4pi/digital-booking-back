package com.digitalbooking.controller;

import com.digitalbooking.model.dto.PolicyTypeDto;
import com.digitalbooking.model.entity.PolicyType;
import com.digitalbooking.model.response.ResponsePayload;
import com.digitalbooking.service.PolicyTypeService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/v1", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PolicyTypeController {
    private final PolicyTypeService service;
    private final ModelMapper modelMapper;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/policy-types")
    public ResponseEntity<ResponsePayload> findAll() {
        return ResponseEntity.ok(new ResponsePayload(service.findAll()));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/policy-types", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> save(@RequestBody @Valid PolicyTypeDto policyTypeDto) {
        ResponsePayload responsePayload =
                new ResponsePayload("El tipo de política ha sido creado",
                        service.save(convertToEntity(policyTypeDto)));

        return new ResponseEntity<>(responsePayload, HttpStatus.CREATED);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = "/policy-types/{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> updateById(@PathVariable Long id, @RequestBody @Valid PolicyTypeDto policyTypeDto) {
        ResponsePayload responsePayload =
                new ResponsePayload("El tipo de política ha sido actualizado",
                        service.updateById(id, policyTypeDto));

        return ResponseEntity.ok(responsePayload);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/policy-types/{id}")
    public ResponseEntity<ResponsePayload> deleteById(@PathVariable Long id) {
        return ResponseEntity.ok(new ResponsePayload("El tipo de política ha sido eliminado"));
    }

    private PolicyType convertToEntity(PolicyTypeDto policyTypeDto) {
        return modelMapper.map(policyTypeDto, PolicyType.class);
    }
}
