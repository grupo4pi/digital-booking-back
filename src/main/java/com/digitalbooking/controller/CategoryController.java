package com.digitalbooking.controller;

import com.digitalbooking.model.dto.CategoryDto;
import com.digitalbooking.model.entity.Category;
import com.digitalbooking.model.response.ResponsePayload;
import com.digitalbooking.service.CategoryService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/v1", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryController {
    private final CategoryService service;
    private final ModelMapper modelMapper;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/categories")
    public ResponseEntity<ResponsePayload> getAll() {
        return ResponseEntity.ok(new ResponsePayload(service.findAll()));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/categories/{id}")
    public ResponseEntity<ResponsePayload> findById(@PathVariable Long id) {
        return ResponseEntity.ok(new ResponsePayload(service.findById(id)));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/categories", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> save(@RequestBody @Valid CategoryDto categoryDto) {
        ResponsePayload responsePayload =
                new ResponsePayload("La categoría ha sido creada",
                        service.save(convertToEntity(categoryDto)));

        return new ResponseEntity<>(responsePayload, HttpStatus.CREATED);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = "/categories/{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> updateById(@PathVariable Long id, @RequestBody @Valid CategoryDto categoryDto) {
        ResponsePayload responsePayload =
                new ResponsePayload("La categoría ha sido actualizada",
                        service.updateById(id, categoryDto));
        return ResponseEntity.ok(responsePayload);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping(path = "/categories/{id}")
    public ResponseEntity<ResponsePayload> deleteById(@PathVariable Long id) {
        service.deleteById(id);
        return ResponseEntity.ok(new ResponsePayload(String.format("La categoría con id %s ha sido eliminada", id.toString())));
    }

    private Category convertToEntity(CategoryDto categoryDto) {
        return modelMapper.map(categoryDto, Category.class);
    }
}