package com.digitalbooking.controller;

import com.digitalbooking.model.dto.ProductDto;
import com.digitalbooking.model.response.ResponsePayload;
import com.digitalbooking.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/v1", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ProductController {
    private final ProductService service;

    @ResponseStatus(HttpStatus.OK)
    @io.swagger.v3.oas.annotations.parameters.RequestBody()
    @GetMapping("/products")
    public ResponseEntity<ResponsePayload> findAll() {
        return ResponseEntity.ok(new ResponsePayload(service.findAll()));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/products/{id}")
    public ResponseEntity<ResponsePayload> findById(@PathVariable Long id) {
        return ResponseEntity.ok(new ResponsePayload(service.findById(id)));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/categories/{categoryId}/products")
    public ResponseEntity<ResponsePayload> findAllByCategoryId(@PathVariable Long categoryId) {
        return ResponseEntity.ok(new ResponsePayload(service.findAllByCategoryId(categoryId)));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/cities/{cityId}/products")
    public ResponseEntity<ResponsePayload> findAllByCityId(@PathVariable Long cityId) {
        return ResponseEntity.ok(new ResponsePayload(service.findAllByCityId(cityId)));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/products", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> save(@RequestBody @Valid ProductDto productDto) {
        ResponsePayload responsePayload = new ResponsePayload(
                "El producto ha sido creado",
                service.save(productDto)
        );

        return new ResponseEntity<>(responsePayload, HttpStatus.CREATED);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/products/{id}")
    public ResponseEntity<ResponsePayload> deleteById(@PathVariable Long id) {
        service.deleteById(id);
        return ResponseEntity.ok(new ResponsePayload(String.format("El producto con id %s ha sido eliminado", id.toString())));
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = "/products/{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> updateById(@PathVariable Long id, @RequestBody @Valid ProductDto productDto) {
        ResponsePayload responsePayload = new ResponsePayload(
                "El producto ha sido actualizado",
                service.updateById(id, productDto)
        );

        return ResponseEntity.ok(responsePayload);
    }
}
