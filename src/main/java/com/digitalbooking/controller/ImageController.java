package com.digitalbooking.controller;

import com.digitalbooking.model.dto.ImageDto;
import com.digitalbooking.model.entity.Image;
import com.digitalbooking.model.response.ResponsePayload;
import com.digitalbooking.service.ImageService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/v1", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ImageController {
    private final ImageService service;
    private final ModelMapper modelMapper;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/products/{productId}/images")
    public ResponseEntity<ResponsePayload> getAllImagesByProductId(@PathVariable Long productId) {
        return ResponseEntity.ok(new ResponsePayload(service.getAllImagesByProductId(productId)));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/products/{productId}/images", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> createImage(@PathVariable Long productId, @RequestBody @Valid ImageDto imageDto) {
        ResponsePayload responsePayload = new ResponsePayload("La imagen ha sido creada", service.createImage(productId, convertToEntity(imageDto)));
        return new ResponseEntity<>(responsePayload, HttpStatus.CREATED);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = "/images/{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> updateById(@PathVariable Long id, @RequestBody @Valid ImageDto imageDto) {
        ResponsePayload responsePayload = new ResponsePayload("La imagen ha sido actualizada", service.updateById(id, imageDto));
        return ResponseEntity.ok(responsePayload);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/images/{id}")
    public ResponseEntity<ResponsePayload> deleteById(@PathVariable Long id) {
        service.deleteById(id);
        return ResponseEntity.ok(new ResponsePayload(String.format("La imagen con id %s ha sido eliminada", id.toString())));
    }

    private Image convertToEntity(ImageDto imageDto) {
        return modelMapper.map(imageDto, Image.class);
    }
}
